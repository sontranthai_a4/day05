# Register Form PHP
## Day06
- [x] Validate file mime (allow image only).
- [x] Create `uploads` folder if it's not exist.
- [x] Named file by `filename`_`YmdHis`.`extension`.
## Day05
```
[drwxrwxr-x]  .
├── [-rw-rw-r--]  confirm.php
├── [drwxrwxr-x]  css
│   └── [-rw-rw-r--]  styles.css
├── [-rw-rw-r--]  README.md
├── [-rw-rw-r--]  register.php
└── [drwxrwxrwx]  uploads

2 directories, 4 files
```